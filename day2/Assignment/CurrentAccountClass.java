public class CurrentAccountClass {

   
    private static long accountNumberTracker = 10000;

    private double accountBalance;

    private String customerName;

    private String emailAddress;

    private String nominee;

    private String GSTNo;

    private String BusinessName;

    private Address address;
    private long accountNumber;


    public long getAccountNumber() {
        return accountNumber;
    }

    /*
     * Rules of constructor: a. Name of the constructor should be the same as the
     * class name b. The constructot should not have a return type c. If you do not
     * provide a custome constructor, the compiler will create a default no argument
     * constructor c. If you provide an explict constructor, the compiler will not
     * create a default constructor
     * 
     */
    public CurrentAccountClass(String customerName, double initialAccountBalance, String GSTNo, String BusinessName, Address address) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.accountNumber = ++ accountNumberTracker;
        this.GSTNo = GSTNo;
        this.BusinessName = BusinessName;
        this.address = address;
    }
    //CONSTRUCTOR OVERLOADING
    public CurrentAccountClass(String customerName, double initialAccountBalance, String GSTNo, String BusinessName) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.accountNumber = ++ accountNumberTracker;
        this.GSTNo = GSTNo;
        this.BusinessName = BusinessName;
    }

    

    public double withdraw(double amount) {
        if(validateAmount(amount)){
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double checkBalance() {
        return this.accountBalance;
    }

    public double deposit(double amount) {
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }
    public boolean validateAmount(double amount){
        return (this.accountBalance-50000) >= amount;
    }

    // public double transferAmount(double amount, SavingsAccount user){
    //     //validation
    //     if(validateAmount(amount)){
    //         //deduct the amount
    //         this.accountBalance-=amount;
    //         user.accountBalance+=amount;
    //     //deposit to user's account
    //     }
        
    // }
    public void transferAmount(double amount, long userAccountId){
        //validation
        CurrentAccountClass userAccount = CurrentAccountRegister.fetchCurrentAccountByAccountId(userAccountId);
        if(userAccount != null){
            if(validateAmount(amount)){
            //deduct the amount
                this.accountBalance-=amount;
                userAccount.accountBalance+=amount;
                System.out.println("The transfer has been done successfully");
            //deposit to user's account
            }
        }
    }

    // public void CommunicationAddress(String streetName, int zipCode, String city) {

    //     this.street = streetName;
    //     this.zipCode = zipCode;
    //     this.city = city;
    // }

    public String getCustomerName() {
        return this.customerName;
    }
}