public class CurrentAccountRegister {

    private static CurrentAccountClass[] currentAccounts = new CurrentAccountClass[] {
            new CurrentAccountClass("Vinay", 34_000, "AEI58850948", "Tech_Industries", new Address("Whitefield", "Bangalore", "Karnataka", 577142),
            new CurrentAccountClass("Rajeesh", 70_000, "AEI5896086", "Automotive_Industries", new Address("8th Ave", "Bangalore", "Karnataka", 577042)))

    };

    public static CurrentAccountClass fetchCurrentAccountByAccountId(long accountId) {

        for (CurrentAccountClass currentAccount : currentAccounts) {
            if (currentAccount.getAccountNumber() == accountId) {
                return currentAccount;
            }
        }
        return null;
    }
}