public class CurrentAccountClient {

    public static void main(String[] args) {
        Address address1 = new Address("Whitefield", "Bangalore", "Karnataka", 577142);
        CurrentAccountClass rajeesh = new CurrentAccountClass("Rajeesh", 300000, "AEI5896086", "Automotive_Industries", address1);
        System.out.println("Customer Name: " + rajeesh.getCustomerName());
        System.out.println("Account Number " + rajeesh.getAccountNumber());
        System.out.println("Initial Account balance " + rajeesh.checkBalance());
        System.out.println("Account Balance " + rajeesh.deposit(50000));
        System.out.println("Balance after deposit" + rajeesh.checkBalance());
        rajeesh.withdraw(30000);
        System.out.println("Balance after Withdraw " + rajeesh.checkBalance());

        System.out.println(" ---------------------------------");

        CurrentAccountClass naveen = new CurrentAccountClass("Naveen", 500000, "AAR7789JKJ", "HSBC_Technologies");
        System.out.println("Account Number " + naveen.getAccountNumber());
        System.out.println("Customer Name: " + naveen.getCustomerName());
        System.out.println("Initial Account balance " + naveen.checkBalance());
        System.out.println("Account Balance " + naveen.deposit(4000));
        System.out.println("Balance after deposit" + naveen.checkBalance());
        naveen.withdraw(2000);
        System.out.println("Balance after Withdraw " + naveen.checkBalance());

        System.out.println(" ---------------------------------");

        System.out.println("Customer Name: " + naveen.getCustomerName());

        rajeesh.transferAmount(100000, 10000);


    }
}