package com.item.client;
import com.item.model.Item;
import com.item.controller.ItemController;

public class ItemClient {
	public static void main(String[] args) {
		ItemController controller = new ItemController();
		Item shirt = controller.registerItem("shirt", 2500);

		Item jeans = controller.registerItem("jeans", 7000);
		
		Item mobile = controller.registerItem("mobile", 40000);

		Item[] allItems = controller.fetchAllItems();
		
//		Item item3 = controller.fetchItemByItemId(3);
//		System.out.println("Item3 is :" + item3.getItemName());
//		Item item2 = controller.fetchItemByItemId(2);
//		System.out.println("Item2 is :" + item2);
//		Item item1 = controller.fetchItemByItemId(1);
//		System.out.println("Item1 is :" + item1);

		for (Item item : allItems) {
			if (item != null) {
				System.out.println("Name of item - " + item.getItemName());
				System.out.println("price of item - " + item.getItemPrice());
			} else {
				break;
			}
		}
		
		Item item3_updated_name = controller.updateNameOfItem(3, "computer");
		
		for (Item item : allItems) {
			if (item != null) {
				System.out.println("Name of item - " + item.getItemName());
				System.out.println("price of item - " + item.getItemPrice());
			} else {
				break;
			}
		}
		
		Item item3_updated_price = controller.updatePriceOfItem(3, 90000);
		
		for (Item item : allItems) {
			if (item != null) {
				System.out.println("Name of item - " + item.getItemName());
				System.out.println("price of item - " + item.getItemPrice());
			} else {
				break;
			}
		}

	}
	
}
