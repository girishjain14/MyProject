package com.item.service;

import com.item.DAO.ItemDAO;
import com.item.model.Item;

public interface ItemService {
	
	
	
	public Item createItem(String itemName, double itemPrice);
	
	public void deleteItem(long itemId);
	
    public Item[] fetchAllItems();
	
	public Item fetchItemByItemId(long itemId);
	
	public Item updateNameOfItem(long itemId, String itemName);
	
	public Item updatePriceOfItem(long itemId, double itemPrice); 
	
	
}
