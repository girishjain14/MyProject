package com.item.service;

import com.item.model.Item;
import com.item.DAO.ItemDAOArrayInit;
import com.item.DAO.ItemDAO;

public class ItemServiceInit implements ItemService{
	
	ItemDAO dao = new ItemDAOArrayInit();

	public Item createItem(String itemName, double itemPrice) {
		Item item = new Item(itemName, itemPrice);
		return this.dao.storeItem(item);
	}
	
	public void deleteItem(long itemId) {
		this.dao.deleteItem(itemId);
	}
	
    public Item[] fetchAllItems() {
    	return this.dao.fetchAllItems();
    }
	
	public Item fetchItemByItemId(long itemId) {
		return this.dao.fetchItemByItemId(itemId);
	}
	
	public Item updateNameOfItem(long itemId, String itemName) {
		Item item = this.dao.fetchItemByItemId(itemId);
		item.setItemName(itemName);
		return this.dao.updateItem(itemId, item);
	}
	
	public Item updatePriceOfItem(long itemId, double itemPrice) {
		Item item = this.dao.fetchItemByItemId(itemId);
		item.setItemPrice(itemPrice);
		return this.dao.updateItem(itemId, item);
	}
	
	
}
