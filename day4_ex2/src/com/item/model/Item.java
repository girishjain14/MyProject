package com.item.model;

public class Item {

		private static int itemTracker = 1;
		private String itemName;
		private long itemId;
		private double itemPrice;
		
		public Item(String itemName, double itemPrice) {
			this.itemId = itemTracker++;
			this.itemName = itemName;
			this.itemPrice = itemPrice;
		}


		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		public long getItemId() {
			return itemId;
		}

		public void setItemId(long itemId) {
			this.itemId = itemId;
		}

		public double getItemPrice() {
			return itemPrice;
		}

		public void setItemPrice(double itemPrice) {
			this.itemPrice = itemPrice;
		}
		
		
		
}
