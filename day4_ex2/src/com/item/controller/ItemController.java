package com.item.controller;
import com.item.model.*;
import com.item.service.*;


public class ItemController {
	ItemService service = new ItemServiceInit();
	
	public Item registerItem(String itemName, double itemPrice) {
		return this.service.createItem(itemName, itemPrice);
	}
	
	public void deleteitem(long itemId) {
		this.service.deleteItem(itemId);
	}
	
	public Item[] fetchAllItems() {
		return this.service.fetchAllItems();
	}
	
	public Item fetchItemByItemId(long itemId) {
		return this.service.fetchItemByItemId(itemId);
	}
	
	public Item updateNameOfItem(long itemId, String itemName) {
		return this.service.updateNameOfItem(itemId, itemName);
	}
	
	public Item updatePriceOfItem(long itemId, double itemPrice) {
		return this.service.updatePriceOfItem(itemId, itemPrice);
	}
}
