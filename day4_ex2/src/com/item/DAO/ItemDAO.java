package com.item.DAO;

import com.item.model.Item; 

public interface ItemDAO {
	
	
	public Item storeItem(Item item);
	
	public Item updateItem(long itemId, Item item);
	
	public void deleteItem(long itemId);
	
	public Item[] fetchAllItems();
	
	public Item fetchItemByItemId(long itemId);
}
