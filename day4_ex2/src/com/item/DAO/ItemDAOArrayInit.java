package com.item.DAO;

import com.item.model.Item;

public class ItemDAOArrayInit implements ItemDAO{
	
	private static Item items[] = new Item[100];
	private static int index = 0;

	@Override
	public Item storeItem(Item item) {
		// TODO Auto-generated method stub
		items[index++] = item;
		return item;
	}

	@Override
	public Item updateItem(long itemId, Item item) {
		// TODO Auto-generated method stub
		for(int index = 0; index < items.length; index++) {
			if(items[index].getItemId() == itemId) {
				items[index] = item;
				return item;
			}
		}
		
		return null;
	}

	@Override
	public void deleteItem(long itemId) {
		// TODO Auto-generated method stub
		for(int index = 0;index < items.length; index++ ) {
			if(items[index].getItemId() == itemId) {
				items[index] = null;
				break;
			}
		}

		
	}

	@Override
	public Item[] fetchAllItems() {
		// TODO Auto-generated method stub
		return items;
	}

	@Override
	public Item fetchItemByItemId(long itemId) {
		// TODO Auto-generated method stub
		for(int index = 0;index < items.length; index++ ) {
			if(items[index].getItemId() == itemId) {
				return items[index];
			}
		}
		return null;

	}
	
}
