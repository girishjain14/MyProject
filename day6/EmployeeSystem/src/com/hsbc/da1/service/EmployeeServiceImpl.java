package com.hsbc.da1.service;

import java.util.TreeSet;

import com.hsbc.da1.DAO.EmployeeDAO;
import com.hsbc.da1.DAO.HashSetDAOImpl;
import com.hsbc.da1.exception.InsufficientLeavesException;
import com.hsbc.da1.modle.Employee;
import java.util.List;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;


public class EmployeeServiceImpl implements EmployeeService{

	EmployeeDAO dao = new HashSetDAOImpl();
	@Override
	public Employee createEmployee(String name, double salary, int age) {
		// TODO Auto-generated method stub
		Employee employee = new Employee(name, salary, age);
		this.dao.storeEmployee(employee);
		return employee;
	}

	@Override
	public void deleteEmployee(int id) {
		// TODO Auto-generated method stub
		this.dao.deleteEmployee(id);
	}

	@Override
	public TreeSet<Employee> listAllEmployees() {
		// TODO Auto-generated method stub
		this.dao.listAllEmployees();
		return null;
	}

	@Override
	public Collection<Employee> fetchEmployeeByName(String name) {
		// TODO Auto-generated method stub
		List <Employee> list = new ArrayList<>();
		Employee employee = this.dao.fetchEmployeeByName(name);
		if(employee != null) {
			list.add(employee);
			return list;
		}
		
		//Employee not found Exception
	}

	@Override
	public Employee fetchEmployeeById(int id) {
		// TODO Auto-generated method stub
		Employee employee = this.dao.fetchEmployeeById(id);
		if(employee != null) {
			return employee;
		}
		return null;
		//Employee not found Exception
	}
	
	@Override
	public int LeaveBalance(int id) {
		Employee employee = this.dao.fetchEmployeeById(id);
		return employee.getRemaining_leaves();
	}

	@Override
	public void applyForLeave(int id, int no_of_leaves) throws InsufficientLeavesException {
		// TODO Auto-generated method stub
		if(no_of_leaves <= 10 && this.LeaveBalance(id) > 0) {
			System.out.println("Leave Granted");
			Employee employee = this.dao.fetchEmployeeById(id);
			employee.setRemaining_leaves(40-no_of_leaves);
			this.dao.updateEmployee(id, employee);
		}
		//Throw Exception
		else
			throw new InsufficientLeavesException("You don't have enough leaves");
	}
	
	
}
