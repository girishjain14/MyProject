package com.hsbc.da1.service;

import java.util.HashSet;
import java.util.TreeSet;

import com.hsbc.da1.DAO.EmployeeDAO;
import com.hsbc.da1.DAO.HashSetDAOImpl;
import com.hsbc.da1.exception.InsufficientLeavesException;
import com.hsbc.da1.modle.*;

public interface EmployeeService {
	
	public Employee createEmployee(String name, double salary, int age);
	
	public void deleteEmployee(int id);
	
	public TreeSet<Employee> listAllEmployees();
	
	public Employee fetchEmployeeByName(String name);
	
	public Employee fetchEmployeeById(int id);
	
	public int LeaveBalance(int id);
	
	public void applyForLeave(int id, int no_of_leaves) throws InsufficientLeavesException;
}
