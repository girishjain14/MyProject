package com.hsbc.da1.controller;

import java.util.TreeSet;

import com.hsbc.da1.modle.Employee;
import com.hsbc.da1.service.EmployeeService;
import com.hsbc.da1.service.EmployeeServiceImpl;
import com.hsbc.da1.exception.*;

public class EmployeeController {
	EmployeeService service = new EmployeeServiceImpl();
	
	public Employee openEmployee(String name, double salary, int age) {
		return this.service.createEmployee(name, salary, age);
	}

	
	public void deleteEmployee(int id) {
		// TODO Auto-generated method stub
		return this.service.deleteEmployee(id);
	}

	
	public TreeSet<Employee> listAllEmployees() {
		// TODO Auto-generated method stub
		return this.service.listAllEmployees();
	}

	
	public Employee fetchEmployeeByName(String name) {
		return this.service.fetchEmployeeByName(name);
	}

	
	public Employee fetchEmployeeById(int id) {
		return this.service.fetchEmployeeById(id);
	}
	
	
	public int LeaveBalance(int id) {
		return this.service.LeaveBalance(id);
	}

	
	public void applyForLeave(int id, int no_of_leaves) throws InsufficientLeavesException{
		this.service.applyForLeave(id, no_of_leaves);
	}
}
