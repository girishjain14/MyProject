package com.hsbc.da1.exception;

public class InsufficientLeavesException extends Exception{
	public InsufficientLeavesException(String message) {
		super(message);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}
}
