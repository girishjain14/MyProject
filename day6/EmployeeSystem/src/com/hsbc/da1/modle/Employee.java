package com.hsbc.da1.modle;

import java.util.Objects;

public class Employee {
	private static int employeeTracker = 1;
	
	private String employee_name;
	
	private double employee_salary;
	
	private int employee_age;
	
	private int employee_id;
	
	private int remaining_leaves;
	
	public Employee(String name, double salary, int age) {
		this.employee_name = name;
		this.employee_salary = salary;
		this.employee_age = age;
		this.employee_id = employeeTracker++;
		this.remaining_leaves = 40;
	}
	
	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public double getEmployee_salary() {
		return employee_salary;
	}

	public void setEmployee_salary(double employee_salary) {
		this.employee_salary = employee_salary;
	}

	public int getEmployee_age() {
		return employee_age;
	}

	public void setEmployee_age(int employee_age) {
		this.employee_age = employee_age;
	}

	public int getRemaining_leaves() {
		return remaining_leaves;
	}

	public void setRemaining_leaves(int remaining_leaves) {
		this.remaining_leaves = remaining_leaves;
	}

	public int getEmployee_id() {
		return employee_id;
	}

	@Override
	public String toString() {
		return "Employee [employee_name=" + employee_name + ", employee_salary=" + employee_salary + ", employee_age="
				+ employee_age + ", employee_id=" + employee_id + ", remaining_leaves=" + remaining_leaves + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(employee_id, employee_name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Employee))
			return false;
		Employee other = (Employee) obj;
		return employee_id == other.employee_id && Objects.equals(employee_name, other.employee_name);
	}		
}
