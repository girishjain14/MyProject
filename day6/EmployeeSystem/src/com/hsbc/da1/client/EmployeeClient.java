package com.hsbc.da1.client;

import com.hsbc.da1.controller.EmployeeController;
import com.hsbc.da1.exception.InsufficientLeavesException;
import com.hsbc.da1.exception.EmployeeNotFoundException;
import com.hsbc.da1.modle.*;

public class EmployeeClient {
	
	public static void main(String[] args) {
		EmployeeController controller = new EmployeeController();
		
		controller.openEmployee("Girish", 50000, 23);
		controller.openEmployee("Vansh", 40000, 24);
		controller.openEmployee("Ram", 70000, 26);
		controller.openEmployee("Madhav", 90000, 29);
		controller.openEmployee("Himanshu", 20000, 43);
		
		Employee employee1 = controller.fetchEmployeeById(1);
		Employee employee2 = controller.fetchEmployeeById(2);
		Employee employee3 = controller.fetchEmployeeById(3);
		Employee employee4 = controller.fetchEmployeeById(4);
		Employee employee5 = controller.fetchEmployeeById(5);
		
		System.out.println("Employee3 Name :" + employee3.getEmployee_name());
		
		
		
		int leaves_left = controller.LeaveBalance(3);
		
		try {
			controller.applyForLeave(3, 9);
		} catch (InsufficientLeavesException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		try {
			controller.applyForLeave(3, 13);
		} catch (InsufficientLeavesException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		
		
		System.out.println(employee1.getEmployee_name());
		System.out.println(leaves_left);
		System.out.println(employee3.getRemaining_leaves());
		
		
	}
}
