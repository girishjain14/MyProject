package com.hsbc.da1.DAO;

import java.util.TreeSet;

import com.hsbc.da1.service.EmployeeService;
import com.hsbc.da1.modle.*;
import java.util.*;

public interface EmployeeDAO {
	
	
	public Employee storeEmployee(Employee employee);
	
	public Employee updateEmployee(int id, Employee employee);
	
	public void deleteEmployee(int id);
	
	public Set<Employee> listAllEmployees();
	
	public Employee fetchEmployeeByName(String name);
	
	public Employee fetchEmployeeById(int id);
}
