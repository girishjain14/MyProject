package com.hsbc.da1.DAO;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;

import com.hsbc.da1.modle.Employee;

public class HashSetDAOImpl implements EmployeeDAO{

	
	public static Set<Employee> Employeeset = new HashSet<>();
	

	
	@Override
	public Employee storeEmployee(Employee employee) {
		// TODO Auto-generated method stub
		Employeeset.add(employee);
		return employee;
	}
	
	public Employee updateEmployee(int id, Employee employee) {
		
		Iterator<Employee> it = Employeeset.iterator();
		while(it.hasNext()) {
			Employee updated = it.next();
			if(updated.getEmployee_id() == id) {
				 updated = employee;
			}
		}
		return employee;
	}


	@Override
	public void deleteEmployee(int id) {
		// TODO Auto-generated method stub
		Iterator<Employee> it = Employeeset.iterator();
		while(it.hasNext()) {
			Employee e = it.next();
			if(e.getEmployee_id() == id) {
				 Employeeset.remove(e);
			}
		}		
	}

	@Override
	public Set<Employee> listAllEmployees() {
		// TODO Auto-generated method stub
		return Employeeset;
	}

	@Override
	public Employee fetchEmployeeByName(String name) {
		// TODO Auto-generated method stub
		Iterator<Employee> it = Employeeset.iterator();

		while(it.hasNext()) {
			Employee e = it.next();
			if(e.getEmployee_name() == name) {
				 return e;
			}
		}	
		return null; //Exception
	}

	@Override
	public Employee fetchEmployeeById(int id) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		Iterator<Employee> it = Employeeset.iterator();

		while(it.hasNext()) {
			Employee e = it.next();
			if(e.getEmployee_id() == id) {
					return e;
			}
		}	
		return null; //Exception
	}

}
