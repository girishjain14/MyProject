public class InsuranceInterfaceClient{
    public static void main(String[] args){
        InsuranceInterface insurance = null;
        int typeofInsurance = Integer.parseInt(args[0]);

        if (typeofInsurance == 1) {
            insurance = new bajajInsurance();
        }
        else{
            insurance = new tataAIG();
        }
        double premium = insurance.calculatePremium("i20", 30000, 412357);
        System.out.println("Insurance Premium is: "+ premium);
        String policy_number = insurance.payPremium(40000, 4145894, 412357);
        System.out.println("Policy Number is: " + policy_number);

        double premium1 = insurance.calculatePremium("baleno", 20000, 412358);
        System.out.println("Insurance Premium is: "+ premium1);
        String policy_number2 = insurance.payPremium(30000, 4145895, 412358);
        System.out.println("Policy Number is: " + policy_number2);

        
    }
}