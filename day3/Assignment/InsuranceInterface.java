interface InsuranceInterface{
    public double calculatePremium(String vehicle_name, double insured_amount, int model_no);
    public String payPremium(double premium_amount, int vehicle_no, int model_no);
}
class bajajInsurance implements InsuranceInterface{
    private static double policies = 1000; 
    public double calculatePremium(String vehicle_name, double insured_amount, int model_no){
        double premium = 0.08*insured_amount;
        return premium;
    }
    public String payPremium(double premium_amount, int vehicle_no, int model_no){
        return "BajajInsurance_"+vehicle_no+"_"+model_no+"_"+policies++;
    }
}
class tataAIG implements InsuranceInterface{
    private static double policies = 1000;
    public double calculatePremium(String vehicle_name, double insured_amount, int model_no){
        double premium = 0.06*insured_amount;
        return premium;
    }
    public String payPremium(double premium_amount, int vehicle_no, int model_no){
        return "BajajInsurance_"+vehicle_no+"_"+model_no+"_"+policies++;
    }
}


