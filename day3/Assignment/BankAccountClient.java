public class BankAccountClient {

    public static void main(String[] args) {
        
        BankAccount bankacc = null;
        int typeofAccount = Integer.parseInt(args[0]);

        if (typeofAccount == 1) {
            Address address1 = new Address("Whitefield", "Bangalore", "Karnataka", 577142);
            bankacc = new CurrentAccount("Rajeesh", 300000, "AEI5896086", "Automotive_Industries", address1);
        }
        else if (typeofAccount == 2) {
            Address address2 = new Address("Ranjit Avenue", "Amritsar", "Punjab", 154901);
            bankacc = new SavingsAccount("Girish", 200000, address2);
        } else {
            Address address3 = new Address("Green Avenue", "Amritsar", "Punjab", 154801);
            bankacc = new SalariedAccount("Vansh", 500000, address3);
        }

        execute(bankacc);
    }

    public static void execute(BankAccount bankacc) {
        /*
         * if (doc instanceof Orthopedecian) { Orthopedecian orhtopedician =
         * (Orthopedecian) doc; orhtopedician.conductCTScan();
         * orhtopedician.conductXRay(); }
         */
        double remaining = bankacc.withdraw(5000);
        System.out.println(remaining);
        bankacc.loan(10000);
        // double amount1 = bankacc.checkBalance();
        // System.out.println("Amount before deposit"+amount1);
        double amount = bankacc.deposit(40000);

        System.out.println("Amount after deposit"+amount);
    }
}
