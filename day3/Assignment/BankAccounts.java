abstract class BankAccount {

    private static long accountNumberTracker = 10000;

    protected double accountBalance;

    private String customerName;

    private String emailAddress;


    private Address address;
    
    private long accountNumber;

    public BankAccount(String customerName, double initialAccountBalance, Address address) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.accountNumber = ++ accountNumberTracker;
        this.address = address;
    }
    public final double deposit(double amount) {
        this.accountBalance = this.accountBalance + amount;
        return accountBalance;
    }
    public final double checkBalance() {
        return this.accountBalance;
    }

    public abstract double withdraw(double amount);
    public abstract void loan(double amount);

     public long getAccountNumber() {
        return accountNumber;
    }
}


class SavingsAccount extends BankAccount {


    //constructor
    public SavingsAccount (String name,double balance, Address address){
        super(name, balance, address);
    }
    public boolean validateAmount(double amount){
        return (this.accountBalance-10000) >= amount;
    }
    public boolean limitChecker(double amount){
        return (amount <= 10000);
    }
    public double withdraw(double amount) {
        if(validateAmount(amount) && limitChecker(amount)){
            this.accountBalance = this.accountBalance - amount;
            return this.accountBalance;
        }
        return 0;
    }
    public void loan(double amount){
        if(amount <= 500000){
            System.out.println("Loan is given");
        }
    }
}

class CurrentAccount extends BankAccount {
   
    private String GSTNo;
    private String BusinessName;
    public CurrentAccount (String name,double balance, String gst, String business, Address address){
        super(name, balance, address);
        this.GSTNo = gst;
        this.BusinessName = BusinessName;
    }
    
    public boolean validateAmount(double amount){
        return (this.accountBalance-25000) >= amount;
    }
    public boolean limitChecker(double amount){
        return true;
    }
    public double withdraw(double amount) {
        if(validateAmount(amount) && limitChecker(amount)){
            this.accountBalance = this.accountBalance - amount;
            return this.accountBalance;
        }
        return 0;
    }
    public void loan(double amount){
        if(amount <= 2500000){
            System.out.println("Loan is given");
        }
    }
}

class SalariedAccount extends BankAccount {

    public SalariedAccount (String name,double balance, Address address){
        super(name, balance, address);
    }
    public boolean validateAmount(double amount){
        return this.accountBalance >= amount;
    }
    public boolean limitChecker(double amount){
        return (amount <= 15000);
    }
    public double withdraw(double amount) {
        if(validateAmount(amount) && limitChecker(amount)){
            this.accountBalance = this.accountBalance - amount;
            return this.accountBalance;
        }
        return 0;
    }
    public void loan(double amount){
        if(amount <= 1000000){
            System.out.println("Loan is given");
        }
    }
}


