public class Matrix {

    public static void main(String[] args) {
        int rows = 5;
        int cols = 5;

        int[][] matrix = new int[rows][cols];

        assignMatrix(matrix);
        printMatrix(matrix);

    }

    public static void assignMatrix(int[][] matrix){
        int initialValue = 10;
        for(int rowindex = 0; rowindex < 5; rowindex++){
            for(int colindex = 0; colindex < 5; colindex++){
                matrix[rowindex][colindex] = initialValue++;
            }
        }
    }

    public static void printMatrix(int[][] matrix){
        for(int rowindex = 0; rowindex < 5; rowindex++){
            for(int colindex = 0; colindex < 5; colindex++){
                system.out.println("\t" + matrix[rowindex][colindex] + "\t");
            }
            System.out.println();
        }
    }
}
