public class TotalBill{
    public static void main(String args[]){
        String str = args[0];
        String stateCode = args[1];


        double billVal = Double.parseDouble(str);

        double totalbill;
        if(stateCode == "KA"){
            totalbill = billVal + (double)(billVal/100)*15;
        }
        else if(stateCode == "TN"){
            totalbill = billVal + (double)(billVal/100)*18;
        }
        else if(stateCode == "MH"){
            totalbill = billVal + (double)(billVal/100)*20;
        }
        else{
            totalbill = billVal + (double)(billVal/100)*12;
        }
        System.out.println("Total Bill is:" + totalbill);
    }
}