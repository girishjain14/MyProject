public class MinMax{
    public static void main(String args[]){
        String firstnum = args[0];
        String secondnum = args[1];
        String thirdnum = args[2];
        String fourthnum = args[3];
        String fifthnum = args[4];

        int integerArray[] = new int[5];
        integerArray[0] = Integer.parseInt(firstnum);
        integerArray[1] = Integer.parseInt(secondnum);
        integerArray[2] = Integer.parseInt(thirdnum);
        integerArray[3] = Integer.parseInt(fourthnum);
        integerArray[4] = Integer.parseInt(fifthnum);

        int maxNumber = 0;
        int minNumber = 1234567890;

        for(int index = 0; index<5; index++){
            if(integerArray[index] > maxNumber){
                maxNumber = integerArray[index];
            }
        }
        

        for(int index = 0; index<5; index++){
            if(integerArray[index] < minNumber){
                minNumber = integerArray[index];
            }

        }

        System.out.println("Maximum number is:" + maxNumber);
        System.out.println("Minimum number is:" + minNumber);
    }
}
