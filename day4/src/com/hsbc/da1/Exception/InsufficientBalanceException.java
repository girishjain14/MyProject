package com.hsbc.da1.Exception;

public class InsufficientBalanceException extends Exception {
	
	public InsufficientBalanceException(String message) {
		super(message);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}
}

