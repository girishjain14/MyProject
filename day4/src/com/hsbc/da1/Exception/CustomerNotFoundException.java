package com.hsbc.da1.Exception;

public class CustomerNotFoundException extends Exception {
	
	public CustomerNotFoundException(String message) {
		super(message);
	}

	public String getMessage() {
		return super.getMessage();
	}
}
