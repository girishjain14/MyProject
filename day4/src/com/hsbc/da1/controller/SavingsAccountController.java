package com.hsbc.da1.controller;
import java.util.List;

import com.hsbc.da1.Exception.InsufficientBalanceException;
import com.hsbc.da1.Exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpl;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

public class SavingsAccountController {

	
	private SavingsAccountService savingsAccountService;
	public SavingsAccountController(SavingsAccountService service) {
		this.savingsAccountService = service;
	}

	public SavingsAccount openSavingsAccount(String customerName, double accountBalance) {
		SavingsAccount savingsAccount = this.savingsAccountService
											.createSavingsAccount(customerName, accountBalance);	
		return savingsAccount;
	}

	public void deleteSavingsAccount(long accountNumber) {
		this.savingsAccountService.deleteSavingsAccount(accountNumber);
	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		return this.savingsAccountService.fetchSavingsAccounts();
	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}

	public double withdraw(long accountId, double amount) throws InsufficientBalanceException, CustomerNotFoundException {
		return this.savingsAccountService.withdraw(accountId, amount);
	}
	
	public double withdrawFromATM(int pin, double amount) throws InsufficientBalanceException, CustomerNotFoundException {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchAccountByPIN(pin);
		return this.savingsAccountService.withdraw(savingsAccount.getAccountNumber(), amount);
	}

	public double deposit(long accountId, double amount) throws CustomerNotFoundException {
		return this.savingsAccountService.deposit(accountId, amount);
	}

	public double checkBalance(long accountId) throws CustomerNotFoundException {
		return this.savingsAccountService.checkBalance(accountId);
	}
	
	public void transfer(long senderId, long toId, double amount) throws InsufficientBalanceException, CustomerNotFoundException {
		this.savingsAccountService.transfer( senderId,  toId,  amount);
	}
}
