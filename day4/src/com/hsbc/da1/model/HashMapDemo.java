package com.hsbc.da1.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.hsbc.da1.model.SavingsAccount;

public class HashMapDemo {
	
	public static void main(String[] args) {
		Map<Integer, SavingsAccount> mapOfSavingsAccount = new TreeMap<>();
		SavingsAccount deepa = new SavingsAccount("Deepa", 45_000);
		SavingsAccount veena = new SavingsAccount("Veena", 55_000);
		SavingsAccount harsha = new SavingsAccount("Harsha", 65_000);
		SavingsAccount vinay = new SavingsAccount("Vinay", 85_000);
		SavingsAccount kuldeep = new SavingsAccount("Kuldeep", 1_05_000);
		
		mapOfSavingsAccount.put(1000, deepa);
		mapOfSavingsAccount.put(1001, veena);
		mapOfSavingsAccount.put(1002, harsha);
		mapOfSavingsAccount.put(1003, vinay);
		mapOfSavingsAccount.put(1004, kuldeep);
		
		
		//System.out.printf("Size of the map is %d %n", mapOfSavingsAccount.size());
		
		//System.out.println(" Is Kuldeep present "+ ( mapOfSavingsAccount.containsKey(1004)));
		
		Set<Integer> setOfKeys = mapOfSavingsAccount.keySet();
		
		Iterator<Integer> keys = setOfKeys.iterator();
		
		while (keys.hasNext()) {
			int key = keys.next();
//			System.out.printf(" Key is %d  %s %n", key, mapOfSavingsAccount.get(key));
		}
		Collection<SavingsAccount> savingsAccounts = mapOfSavingsAccount.values();
		
		Iterator<SavingsAccount> its = savingsAccounts.iterator();
		
		Set<Map.Entry<Integer, SavingsAccount>> setOfEntries = mapOfSavingsAccount.entrySet();
		
		Iterator<Map.Entry<Integer, SavingsAccount>> iter = setOfEntries.iterator();
		
		while(iter.hasNext()) {
			Map.Entry<Integer, SavingsAccount> entry = iter.next();
			System.out.printf(" Key is %d  and value is %s %n", entry.getKey(), entry.getValue().getCustomerName());
		}
	}
}
