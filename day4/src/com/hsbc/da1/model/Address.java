package com.hsbc.da1.model;

public class Address {
	private int zipCode;
	
	private String streetName;
	
	private String city;
	
	public Address(int zipCode, String streetName, String city) {
		this.zipCode = zipCode;
		this.streetName = streetName;
		this.city = city;
	}
}
