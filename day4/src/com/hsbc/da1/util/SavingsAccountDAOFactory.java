package com.hsbc.da1.util;

import com.hsbc.da1.dao.ArrayBackendSavingsAccountDAOImpl;
import com.hsbc.da1.dao.ArrayListackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.LinkedListSavingsAccountDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;

public class SavingsAccountDAOFactory {
	
	public static SavingsAccountDAO getSavingsAccountDAO( int value) {
		if (value == 1 ) {
			return new ArrayBackendSavingsAccountDAOImpl();
		} else if(value == 2) {
			return new ArrayListackedSavingsAccountDAOImpl();
		}else {
			return new LinkedListSavingsAccountDAOImpl();
		}
	}

}
