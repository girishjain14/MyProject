package com.hsbc.da1.client;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.util.SavingsAccountDAOFactory;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

import static java.lang.System.*;
import com.hsbc.da1.Exception.InsufficientBalanceException;
import com.hsbc.da1.Exception.CustomerNotFoundException;

import static java.lang.System.*;
import java.util.Scanner;

public class SavingsAccountClient {
	
	public static void main(String[] args) throws InsufficientBalanceException, CustomerNotFoundException {
		
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please enter your option ==> ");
		System.out.println("1 => Array Backed");
		System.out.println("2 => List Backed");
		System.out.println("3 => Set Backed");
		
		System.out.println("======================================");
		
		int option = scanner.nextInt();
		
		System.out.println(" Entered option is =  "+ option);

		
		//wiring
		SavingsAccountDAO dao = SavingsAccountDAOFactory.getSavingsAccountDAO(option);
		
		SavingsAccountService service = SavingsAccountServiceFactory.getInstance(dao);	
		
		SavingsAccountController controller = new SavingsAccountController(service);
		
		SavingsAccount kiranSavingsAccount = controller.openSavingsAccount("Kiran", 25_000);
		SavingsAccount rajeshSavingsAccount = controller.openSavingsAccount("Rajesh", 45_000);
		
		try {
		controller.transfer(78787, 
				            rajeshSavingsAccount.getAccountNumber(), 45000);
		} catch(InsufficientBalanceException | CustomerNotFoundException exception) {
			System.out.println("Invalid customer Id used for transaction");
		}
		
		out.println("Account Id "+ kiranSavingsAccount.getAccountNumber());
		out.println("Account Id "+ rajeshSavingsAccount.getAccountNumber());
		
		/*double newAccBalance = controller.deposit(1001, 20000);
		out.println("Account Balance: " + newAccBalance);
		
		SavingsAccount[] savingsAccounts = controller.fetchSavingsAccounts();
		
		for(SavingsAccount savingsAccount: savingsAccounts) {
			if ( savingsAccount != null) {
				out.println("Account : "+ savingsAccount);
				
			}
		}*/
	}
}
