package com.hsbc.da1.dao;

import java.util.LinkedList;
import java.util.List;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.Exception.CustomerNotFoundException;

public class LinkedListSavingsAccountDAOImpl implements SavingsAccountDAO {
	List<SavingsAccount> savingsAccounts = new LinkedList<>();

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		savingsAccounts.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		for(SavingsAccount acc : savingsAccounts) {
			if(acc.getAccountNumber() == accountNumber) {
				acc = savingsAccount;
			}
		}
		return savingsAccount;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		for(SavingsAccount acc : savingsAccounts) {
			if(acc.getAccountNumber() == accountNumber) {
				savingsAccounts.remove(acc);
			}
		}
		
	}

	@Override
	public List<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return this.savingsAccounts;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		for(SavingsAccount acc : savingsAccounts) {
			if(acc.getAccountNumber() == accountNumber) {
				return acc;
			}
		}
		throw new CustomerNotFoundException("Account doesn't exist");
	}
	
	

}
